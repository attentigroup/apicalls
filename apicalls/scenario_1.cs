﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using log4net;
using apicalls.Agency;
using apicalls.Equipment;

namespace apicalls
{
    public class scenario_1
    {
        private String AgencyEndPointAddress = ConfigurationManager.AppSettings["AgencyEndPointAddress"];
        private String AgencySvc = "WSHttpBinding_IAgency";
        private String EquipmentEndPointAddress = ConfigurationManager.AppSettings["EquipmentEndPointAddress"];
        private String EquipmentSvc = "WSHttpBinding_IEquipment";

        private static readonly ILog Log =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool execute()
        {
            Log.Info("1. Run API GetAgencies - loop on AgencyID");
            int[] agenciesArray = GetAgencies();


            // 3. Run API GetEquipment_ByAgencyID 
            Log.Info("3. Run API GetEquipment_ByAgencyID");
            bool bRet = true;
            foreach (int agencyID in agenciesArray)
            {
                if (!GetEquipment_ByAgencyID(agencyID))
                    bRet = false;
            }
            return bRet;
        }

        private bool GetEquipment_ByAgencyID(int agencyID)
        {
            Log.Debug("GetEquipment_ByAgencyID for agencyID: " + agencyID);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var equipment = new Equipment.EquipmentClient(EquipmentSvc, EquipmentEndPointAddress);
            equipment.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
            equipment.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["Password"];

            EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest = new EntMsgGetEquipmentListRequest();
            entMsgGetEquipmentListRequest.AgencyID = agencyID;
            try
            {
                EntMsgGetEquipmentListResponse entMsgGetEquipmentListResponse = equipment.GetEquipmentList(entMsgGetEquipmentListRequest);
                Log.Debug("Num of equipments is: " + entMsgGetEquipmentListResponse.EquipmentList.Length);
            }
            catch(FaultException<Equipment.ValidationFault> fe)
            {
                Log.Fatal(fe.Detail.Errors[0].Message);
            }
            return true;

        }

        public int[] GetAgencies()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var agency = new Agency.AgencyClient(AgencySvc, AgencyEndPointAddress);
            agency.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
            agency.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["Password"];

            EntMsgGetAgenciesRequest ent = new EntMsgGetAgenciesRequest();
            ent.AgencyID = 0;
            int[] AgencyIDArray = null;
            try
            {
                EntMsgGetAgenciesResponse res = agency.GetAgencies(ent);
                AgencyIDArray = new int[res.AgenciesList.Length];
                Log.Info("Number of agencies is: " + res.AgenciesList.Length);

                for (int i = 0; i < res.AgenciesList.Length; i++)
                {
                    AgencyIDArray[i] = res.AgenciesList[i].ID;
                }
            }
            catch (FaultException<Agency.ValidationFault> fe)
            {
                Log.Fatal(fe.Detail.Errors[0].Message);
            }

            return AgencyIDArray;
        }
    }
}
