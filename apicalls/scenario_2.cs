﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using apicalls.Offenders;
using apicalls.Events;
using System.ServiceModel;
using System.Configuration;
using log4net;


namespace apicalls
{
    public class scenario_2
    {
        private String OffendersEndPointAddressOffenders = ConfigurationManager.AppSettings["OffendersEndPointAddressOffenders"];
        private String OffendersSvc = "WSHttpBinding_IOffenders";
        private String EventsEndPointAddressEvents = ConfigurationManager.AppSettings["EventsEndPointAddressEvents"];
        private String EventsSvc = "WSHttpBinding_IEvents";

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool execute()
        {
            Log.Info("1. Run API GetAgencies - loop on AgencyID");
            int[] offendersArray = GetOffenders();

            Log.Info("3. Run API GetEvents per each offender_ID");
            foreach (int offenderID in offendersArray)
            {
                Log.DebugFormat("Start GetEvents of OffenderID: {0}", offenderID);
                int[] EventsID = GetEvents(offenderID, null, null);
                Log.DebugFormat("Num of Events for OffenderID: {0} is {1}", offenderID, EventsID.Length);

                Log.DebugFormat("Start GetHandlingActionsByQuery of OffenderID: {0}", offenderID);
                int nNumActions = GetHandlingActionsByQuery(EventsID, null, null);
                Log.DebugFormat("Num of Actions for OffenderID: {0} is {1}", offenderID, nNumActions);
            }
            Log.Info("End execute");

            return true;
        }

        public int[] GetEvents(int offenderID, DateTime? startTime, DateTime? endTime)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var events = new Events.EventsClient(EventsSvc, EventsEndPointAddressEvents);
            events.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
            events.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["Password"];
            EntMsgGetEventsRequest entMsgGetEventsRequest = new EntMsgGetEventsRequest();
            entMsgGetEventsRequest.OffenderID = offenderID;

            if (startTime == null || endTime == null)
                GetStartTime_EndTime(ref startTime, ref endTime);

            entMsgGetEventsRequest.StartTime = startTime;
            entMsgGetEventsRequest.EndTime = endTime;

            try
            {
                EntMsgGetEventsResponse entMsgGetEventsResponse = events.GetEvents(entMsgGetEventsRequest);
                int nNumOfEvents = entMsgGetEventsResponse.EventsList.Length;
                int[] EventsID = new int[nNumOfEvents];

                for (int i = 0; i < nNumOfEvents; i++)
                {
                    EventsID[i] = entMsgGetEventsResponse.EventsList[i].ID;
                }

                return EventsID;
            }
            catch (FaultException<Events.ValidationFault> fe)
            {
                Log.Fatal(fe.Detail.Errors[0].Message);
            }
            return null;
        }

        public int GetHandlingActionsByQuery(int[] eventsID, DateTime? eventFromTime, DateTime? eventToTime)
        {
            if (eventsID == null || eventsID.Length == 0)
                return 0;

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var events = new Events.EventsClient(EventsSvc, EventsEndPointAddressEvents);
            events.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
            events.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["Password"];

            EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest = new EntMsgGetHandlingActionsByQueryRequest();
            entMsgGetHandlingActionsRequest.EventIDList = eventsID;

            if (eventFromTime == null || eventToTime == null)
                GetStartTime_EndTime(ref eventFromTime, ref eventToTime);

            entMsgGetHandlingActionsRequest.EventFromTime = eventFromTime;
            entMsgGetHandlingActionsRequest.EventToTime = eventToTime;

            try
            {
                EntMsgGetHandlingActionsByQueryResponse entMsgGetHandlingActionsByQueryResponse = events.GetHandlingActionsByQuery(entMsgGetHandlingActionsRequest);
                Log.DebugFormat("Num of actions of events is {0}", entMsgGetHandlingActionsByQueryResponse.ActionsList.Length);
                int nNumOfActions = entMsgGetHandlingActionsByQueryResponse.ActionsList.Length;
                return nNumOfActions;
            }
            catch (FaultException<Events.ValidationFault> fe)
            {
                Log.Fatal(fe.Detail.Errors[0].Message);
            }

            return 0;
        }

        private void GetStartTime_EndTime(ref DateTime? startTime, ref DateTime? endTime)
        {
            DateTime localTime = DateTime.Now;
            endTime = localTime;
            TimeSpan ts = new TimeSpan(12, 0, 0);
            startTime = localTime.Subtract(ts);
        }

        public int[] GetOffenders()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var offenders = new Offenders.OffendersClient(OffendersSvc, OffendersEndPointAddressOffenders);
            offenders.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
            offenders.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["Password"];

            EntMsgGetOffendersListRequest entMsgGetOffendersListRequest = new EntMsgGetOffendersListRequest();
            entMsgGetOffendersListRequest.AgencyID = new EntNumericParameter();

            var OffenderIDList = new List<int>();

            int[] idAgenciesArray = GetAgenciesIDFromConfig();
            foreach (int agencyID in idAgenciesArray)
            {
                entMsgGetOffendersListRequest.AgencyID.Value = agencyID;
                entMsgGetOffendersListRequest.IncludeArchived = true;
                try
                {
                    var offendersList = offenders.GetOffenders(entMsgGetOffendersListRequest);
                    Log.InfoFormat("Num of Offenders of agancyID {0} is {1}", agencyID, offendersList.OffendersList.Length);

                    for (int i = 0; i < offendersList.OffendersList.Length; i++)
                    {
                        OffenderIDList.Add(offendersList.OffendersList[i].ID);
                    }
                }
                catch (FaultException<Offenders.ValidationFault> fe)
                {
                    Log.Fatal(fe.Detail.Errors[0].Message);
                }
            }

            int nNumOfOffenders = OffenderIDList.Count();
            Log.DebugFormat("Num of total offenders is: {0}", nNumOfOffenders);
            int[] allOffenderList = new int[nNumOfOffenders];
            for (int i = 0; i < nNumOfOffenders; i++)
            {
                allOffenderList[i] = OffenderIDList[i];
                Log.DebugFormat("OffenderID: {0}", OffenderIDList[i]);
            }

            return allOffenderList;
        }

        public int[] GetAgenciesIDFromConfig()
        {
            String agenciesArray = ConfigurationManager.AppSettings["AgenciesID"];
            Log.DebugFormat("agenciesArray: {0}", agenciesArray);
            int[] idAgenciesArray = Array.ConvertAll(agenciesArray.Split(','), int.Parse);
            return idAgenciesArray;
        }
    }
}
