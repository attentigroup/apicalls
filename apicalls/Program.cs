using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;


namespace apicalls
{
    class Program
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Log.Info("Please enter a numeric argument (1,2).");
                return 1;
            }

            long nArg0 = long.Parse(args[0]);
            switch (nArg0)
            {
                case 1:
                    Log.Info("starting scenario_1");
                    scenario_1 sc1 = new scenario_1();
                    sc1.execute();
                    Log.Info("end scenario_1");
                    break;

                case 2:
                    Log.Info("starting scenario_2");
                    scenario_2 sc2 = new scenario_2();
                    sc2.execute();
                    break;

                default:
                    Console.WriteLine("exit program");
                    break;
            }
            Log.Info("exit");
            return 0;
        }
    }
}
