﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using apicalls;
using apicalls.Agency;
using apicalls.Equipment;

namespace UT_apicalls
{
    [TestClass]
    public class UnitTest_scenario_1
    {
        [TestMethod]
        public void Test_GetAgencies()
        {
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            scenario_1 sc1 = new scenario_1();
            int[] AgencyIDArray = sc1.GetAgencies();
            Assert.AreEqual(/*8*/450, AgencyIDArray.Length);
 
        }

        [TestMethod]
        public void Test_GetEquipment()
        {
            PrivateObject obj = new PrivateObject(typeof(scenario_1));
            // Check how works with private methods
            bool bRet = Convert.ToBoolean(obj.Invoke("GetEquipment_ByAgencyID", 2));
            Assert.IsTrue(bRet);
        }

        [TestMethod]
        public void Test_scenario_1_execute()
        {
            scenario_1 sc1 = new scenario_1();
            bool bRet = sc1.execute();
            Assert.IsTrue(bRet);
        }

        [TestMethod]
        public void Test_Get_Offenders()
        {
            scenario_2 sc2 = new scenario_2();
            int[] offendersList = sc2.GetOffenders();
            Assert.IsTrue(offendersList.Length > 0);
        }

        [TestMethod]
        public void Test_Get_Events()
        {
            scenario_2 sc2 = new scenario_2();

            // QA - DB
            /*
            DateTime startTime = new DateTime(2020, 3, 15);
            DateTime endTime = new DateTime(2020, 3, 17);
            int[] eventsList = sc2.GetEvents(25157, startTime, endTime);
            */
            // Argentine ub DB
            DateTime startTime = new DateTime(2020, 4, 5);
            DateTime endTime = new DateTime(2020, 4, 7);
            int[] eventsList = sc2.GetEvents(2887489, startTime, endTime);

            Assert.AreEqual(/*5*/58, eventsList.Length);
        }

        [TestMethod]
        public void Test_GetHandlingActionsByQuery()
        {
            scenario_2 sc2 = new scenario_2();
            // QA - DB
            /*
            DateTime eventFromTime = new DateTime(2020, 3, 1);
            DateTime eventToTime = new DateTime(2020, 3, 10);
            int[] eventsList = new int[] {2417902, 2417903, 2417904, 2417905, 2417906, 2417907 };
            */
            // QA - DB
            DateTime eventFromTime = new DateTime(2020, 3, 1);
            DateTime eventToTime = new DateTime(2020, 3, 2);
            
            int[] eventsList = new int[] { 258909246, 258909563,  258909622, 258909129};
            int nActions = sc2.GetHandlingActionsByQuery(eventsList, eventFromTime, eventToTime);
            Assert.AreEqual(/*12*/19, nActions);
        }

        [TestMethod]
        public void Test_scenario_2_execute()
        {
            scenario_2 sc2 = new scenario_2();
            bool bRet = sc2.execute();
            Assert.IsTrue(bRet);
        }
    }
}
